package com.example.controller;

import com.example.service.TASK2;
import com.example.service.TASK4;
import com.example.service.TASK1;
import com.example.service.TASK3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TASK1 task1;

    @Autowired
    private TASK2 task2;

    @Autowired
    private TASK3 task3;

    @Autowired
    private TASK4 task4;

    @RequestMapping(value = {"/1", "/1/{word}"}, method = RequestMethod.GET)
    public ResponseEntity<String> PalindromeTask(@PathVariable(value = "word", required = false) String word)
    {
        if(word == null) return new ResponseEntity<>("Digite uma palavra.", HttpStatus.BAD_REQUEST);

            String result = task1.stringIsAPalindrome(word);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }

    @RequestMapping(value = "/2", method = RequestMethod.GET)
    public ResponseEntity<String> removeFromTheMiddle()
    {
        String result = task2.removeFromTheMiddleAndPrint();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @RequestMapping(value = {"/3"}, method = RequestMethod.GET)
    public ResponseEntity<String> DistinctStringTask()
    {
        String result = task3.printHowManyDistinctItensExist();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @RequestMapping(value = "/4", method = RequestMethod.GET)
    public ResponseEntity<String> getAllEmploy() {
        try {
            return new ResponseEntity<>(task4.getAllEmployer(), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>("Não foi possivel completar. " + e, HttpStatus.BAD_REQUEST);
        }
    }

}
