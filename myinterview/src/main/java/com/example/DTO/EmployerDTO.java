package com.example.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@JsonIgnoreProperties
public class EmployerDTO {
    private int emp_no;
    private char gender;
    private String first_name;
    private String last_name;

    @Override
    public String toString() {
        return "EmployerDTO [emp_no =" + emp_no +
                ", gender =" +
                gender +
                ", first_name=" +
                first_name +
                ", last_name=" +
                last_name +"]";
    }

    public int getEmp_no() {
        return emp_no;
    }

    public void setEmp_no(int emp_no) {
        this.emp_no = emp_no;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
