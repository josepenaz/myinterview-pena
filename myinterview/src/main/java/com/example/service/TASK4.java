package com.example.service;


import com.example.DTO.EmployerDTO;
import com.example.service.CreateJSONFile;
import com.example.service.S3BucketObjectUpload;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.google.gson.Gson;
import java.util.ArrayList;


/**
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint=> https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda 
 * AWS s3 bucket => interview-digiage
 *
 */
@Service
public class TASK4 {
    public String getAllEmployer() {
        int maleSum = 0;
        int femaleSum = 0;

        CreateJSONFile createJSONFile = new CreateJSONFile();

        Gson gson = new Gson();
        ArrayList<Integer> knownEmp_no = new ArrayList();
        S3BucketObjectUpload s3Bucket = new S3BucketObjectUpload();
        RestTemplate restTemplate = new RestTemplate();

        try {
            EmployerDTO[] responseObject = restTemplate.getForObject("https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda", EmployerDTO[].class);
            ArrayList<EmployerDTO> tempArray = new ArrayList<>();

            for (int i = 0; i < responseObject.length; i++) {
                if (!knownEmp_no.contains(responseObject[i].getEmp_no())) {
                    knownEmp_no.add(responseObject[i].getEmp_no());
                    tempArray.add(responseObject[i]);
                    if (responseObject[i].getGender() == 'M')
                        maleSum++;
                    else
                        femaleSum++;
                }
            }

            String json = gson.toJson(tempArray);
            createJSONFile.createTransferFile(json);
            s3Bucket.uploadBucketObject(json);

        }catch (Exception e){
            throw new Error(e);
        }

        return "There are " + femaleSum + " and " + maleSum + " male records";

    }
}