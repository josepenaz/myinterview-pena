package com.example.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
@Service
public class TASK3 {
    public String printHowManyDistinctItensExist() {
        ArrayList<Character> listOfString = new ArrayList<>();

        Random generator = new Random();
        String AlphaNumerics = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for(int i = 0; i < generator.nextInt(100); i++){
            listOfString.add(AlphaNumerics.charAt(generator.nextInt(AlphaNumerics.length())));
        }

        List<Character> distinctCharacter = listOfString.stream().distinct().collect(Collectors.toList());

        return "List: " + listOfString + "\n Distinct Characteres: " + distinctCharacter.size() +" " +distinctCharacter;

    }

}
