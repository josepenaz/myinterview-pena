package com.example.service;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 *
 */
@Service
public class TASK2 {
    public String removeFromTheMiddleAndPrint(){

        LinkedList lList = new LinkedList();
        LinkedList afterDelete = new LinkedList();

        lList.add(1);
        lList.add(2);
        lList.add(3);
        lList.add(4);
        lList.add(5);
        lList.add(6);
        lList.add(7);


        afterDelete = (LinkedList) lList.clone();

        System.out.println(lList);
        if(lList.size() % 2 == 0)
            afterDelete.remove(lList.size() / 2);
        else
            afterDelete.remove((lList.size() - 1) / 2);

        System.out.println(afterDelete);

    return "Linked List before delete " + lList + " and Linked List after delete the element at the middle: " + afterDelete;
    }
}
