package com.example.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateJSONFile {
    public void createTransferFile(String employerDTO) throws IOException {
        try {

            File file = new File("myinterview/src/main/resources/JsonFile.json");
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(employerDTO);
            fileWriter.flush();
            fileWriter.close();

        }catch (IOException e){
            throw new IOException("Falha na criação do arquivo." + e);
        }
    }
}
