package com.example.service;

import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
@Service
public class TASK1 {
    public String stringIsAPalindrome(String word){
            System.out.println(word);
            if(word == "") throw new IllegalArgumentException();


                String aux = new StringBuilder(word).reverse().toString();
                if (word.equals(aux)) {
                    return "Is a Palindrome";
                } else {
                    return "Is not a Palindrome";
                }

    }
}
